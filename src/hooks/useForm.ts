import { useState } from "react";

interface Props {
    initial: Object; // TODO: create default type
    validations?: Record<string, (value: string | number) => string | undefined>; // TODO: create validations type
}

interface Form {
    [k: string]: {
        value: any;
        error: string;
    };
}

function useForm(props: Props): [Form, (name: string) => (value: string | number) => void] {
    const { initial, validations } = props;

    const { fromEntries, entries } = Object
    const [form, setForm] = useState(fromEntries(entries(initial)
        .map(([key, value]) => [key, {
            value,
            error: ''
        }])))

    const setValue = (name: string) => (value: string | number) => {
        const error = (validations && validations[name] && validations[name](value)) || ''
        setForm((currentForm) => ({
            ...currentForm,
            [name]: {
                value,
                error
            }
        }))
    }

    return [form, setValue]
}

export default useForm;
