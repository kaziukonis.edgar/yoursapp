import { Octokit } from "octokit";

const octokit = new Octokit()

export const getIssues = (
    owner: string,
    repo: string,
    state?: 'closed' | 'open' | 'all',
    page?: number,
    per_page?: number
) => octokit.request('GET /repos/{owner}/{repo}/issues', {
    accept: 'application/vnd.github.v3+json',
    owner,
    repo,
    state,
    page,
    per_page
})

export const getRepo = (owner: string, repo: string) => octokit.request('GET /repos/{owner}/{repo}', {
    accept: 'application/vnd.github.v3+json',
    owner,
    repo,
})