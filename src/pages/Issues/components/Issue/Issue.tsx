import styles from './Issue.module.scss';
import classNames from 'classnames/bind'
import Open from '../../../../assets/Open'
import Complete from '../../../../assets/Complete'
import Chat from '../../../../assets/Chat'

const cx = classNames.bind(styles);

interface Props {
	state: string,
	title: string,
	subtitle: string,
	commentsCount?: number
}

const issueState = (state: string) => {
	switch (state) {
		case 'open':
			return <Open />
		default:
			return <Complete />
	}
}

function Issue(props: Props) {
	const { title, subtitle, state, commentsCount } = props;
	return (
		<div className={cx('Issue')}>
			<div className={cx('Issue__container')}>
				<div className={cx('Issue__state')} >
					{issueState(state)}
				</div>
				<div className={cx('Issue__text')}>
					<div className={cx('Issue__title')}>
						{title}
					</div>
					<div className={cx('Issue__subtitle')}>
						{subtitle}
					</div>
				</div>
				<div className={cx('Issue__comments')}>
					{!!commentsCount && <Chat />}
					{!!commentsCount &&
						<span className={cx('Issue__count')}>
							{commentsCount}
						</span>
					}
				</div>
			</div>
		</div>
	);
}

export default Issue;
