import styles from './Issues.module.scss';
import classNames from 'classnames/bind'
import Issue from './components/Issue';
import Open from '../../assets/Open'
import Complete from '../../assets/Complete'
import React, { useEffect, useState } from 'react';
import Pagination from '../../components/Pagination';
import { getIssuesAction } from '../../store/issues/actions';
import { connect } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { IssuesAction, IssuesState } from '../../store/issues/types';
import { RepoState } from '../../store/repo/types';
import { getIssues } from '../../services/github';

const cx = classNames.bind(styles);

interface Props {
	getIssues: typeof getIssues,
	issuesReducer: IssuesState,
	repoReducer: RepoState
}

const Issues: React.FC<any> = (props: Props) => {
	const navigate = useNavigate();
	const [filter, setFilter] = useState<'open' | 'closed'>('open');
	const [page, setPage] = useState(1);

	useEffect(() => {
		if (!props.repoReducer.repo) {
			navigate('/')
		}
	}, [props.repoReducer.repo])

	const { name, owner: { login }, open_issues_count, total_count } = props.repoReducer.repo || { owner: {} };

	useEffect(() => {
		if (login && name) {
			props.getIssues(login, name, filter, page)
		}
	}, [filter, page])


	const onFilterChange = (open?: boolean) => {
		if (props.repoReducer.loading) return
		if (open) {
			setFilter('open')
		} else {
			setFilter('closed')
		}
		setPage(1);
	}

	return (
		<div className={cx('Issues')}>
			<div className={cx('Issues__title')}>Issues <span className={cx('Issues__issues-count')}>{total_count}</span></div>
			<div className={cx('Issues__container')}>
				<div className={cx('Issues__filters')}>
					<div
						onClick={() => onFilterChange(true)}
						className={cx([
							'Issues__filters--open',
							{ 'Issues__filters--open--active': filter === 'open' }
						])}
					>
						<Open />
						<span>{open_issues_count} Open</span>
					</div>
					<div
						onClick={() => onFilterChange()}
						className={cx([
							'Issues__filters--closed',
							{ 'Issues__filters--closed--active': filter === 'closed' }
						])}
					>
						<Complete />
						<span>Closed</span>
					</div>
				</div>
				{
					props.issuesReducer.issues &&
					props.issuesReducer.issues.map((issue) => (
						<Issue
							key={issue.id}
							title={issue.title}
							state={issue.state}
							subtitle={`#${issue.number} opened at ${new Date(issue.created_at).toLocaleDateString('lt')} by ${issue.user?.login}`}
							commentsCount={issue.comments}
						/>
					))
				}
			</div>
			{
				Number(props.issuesReducer.numberOfPages) > 1 &&
				<Pagination selectedPage={page} onChange={setPage} pagesCount={Number(props.issuesReducer.numberOfPages)} />
			}
		</div>
	);
}

const mapDispatchToProps = (dispatch: React.Dispatch<IssuesAction>) => ({
	getIssues: getIssuesAction(dispatch),
});

const mapStateToProps = (state: IssuesAction) => {
	return state
};

export default connect(mapStateToProps, mapDispatchToProps)(Issues);