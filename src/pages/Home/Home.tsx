import styles from './Home.module.scss';
import classNames from 'classnames/bind'
import Input from '../../components/Input';
import Background from '../../components/Background';
import ButtonCustom from '../../components/Button';
import Tooltip from '../../components/Tooltip';
import useForm from '../../hooks/useForm';
import { connect } from 'react-redux';
import { getIssuesAction } from '../../store/issues/actions';
import { getRepoAction } from '../../store/repo/actions';
import { useNavigate } from 'react-router-dom';
import { Dispatch } from 'react';
import { IssuesAction, IssuesState } from '../../store/issues/types';
import { RepoAction, RepoState } from '../../store/repo/types';
import { getIssues, getRepo } from '../../services/github';

const cx = classNames.bind(styles);

interface Props {
	getIssues: typeof getIssues,
	getRepo: typeof getRepo,
	issuesReducer: IssuesState,
	repoReducer: RepoState
}

const Home: React.FC<any> = (props: Props) => {
	const navigate = useNavigate()
	const [form, setValue] = useForm({
		initial: {
			owner: '',
			repository: ''
		},
		validations: {
			owner: (value) => {
				if (!value) {
					return 'Field is required'
				}
			},
			repository: (value) => {
				if (!value) {
					return 'Field is required'
				}
			}
		}
	})

	const { repository, owner } = form;

	const showIssues = async () => {
		await Promise.all([
			props.getIssues(owner.value, repository.value),
			props.getRepo(owner.value, repository.value)
		])

		if (!props.issuesReducer.error) {
			navigate('/issues');
		}
	}

	return (
		<div className={cx('Home')}>
			<div className={cx('Home__container')}>
				<div className={cx('Home__form')}>
					<Tooltip
						message={owner.error}
						type='error'
						visible={!!owner.error}
					>
						<Input
							onChange={setValue('owner')}
							placeholder='Owner'
							name="owner"
							label='Owner'
							required
							isValid={!owner.error && owner.value}
							isError={!!owner.error}
						/>
					</Tooltip>
					<Tooltip
						message={repository.error}
						type='error'
						visible={!!repository.error}
					>
						<Input
							onChange={setValue('repository')}
							placeholder='Repository name'
							name="repository"
							label='Repository'
							required
							isValid={!repository.error && repository.value}
							isError={!!repository.error}
						/>
					</Tooltip>
				</div>
				<Tooltip
					message={'Not found'}
					type='error'
					visible={!!props.issuesReducer.error}
				>
					<ButtonCustom
						disabled={(!!owner.error || !!repository.error || !repository.value || !owner.value)}
						onClick={showIssues}
						className={cx('Home__show-issues-button')}
					>
						Show issues
					</ButtonCustom>
				</Tooltip>
			</div>
			<Background />
		</div>
	);
}

const mapDispatchToProps = (dispatch: Dispatch<IssuesAction | RepoAction>): any => ({
	getIssues: getIssuesAction(dispatch),
	getRepo: getRepoAction(dispatch)
});

const mapStateToProps = (state: { issuesReducer: IssuesState, repoReducer: RepoState }) => {
	return state
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
