import { Dispatch } from "react";
import { getIssues, getRepo } from "../../services/github";
import { ActionTypes, IssuesAction } from "./types";

export const getIssuesAction = (dispatch: Dispatch<IssuesAction>) => async (
  owner: string,
  repository: string,
  state: 'closed' | 'open' | 'all' = 'open',
  page: number = 1,
  perPage: number = 30,
) => {
  dispatch({ type: ActionTypes.REQUEST_GET_ISSUES });
  try {
    const response = await getIssues(owner, repository, state, page, perPage)

    const { data: repoData } = await getRepo(owner, repository);
    const { data: lastIssueData } = await getIssues(owner, repository, 'all', 1, 1)

    const total_count = lastIssueData.length > 0 ? lastIssueData[0].number : 0;
    const closed_issues_count = total_count - repoData.open_issues_count;

    const number_of_pages = numberOfPages(
      state,
      perPage,
      {
        total_count,
        closed_issues_count,
        open_issues_count: repoData.open_issues_count
      }
    )

    return dispatch({ type: ActionTypes.SUCCESS_GET_ISSUES, ...response, number_of_pages });
  } catch (err: unknown) {
    return dispatch({ type: ActionTypes.ERROR_GET_ISSUES, error: err });
  }
}

const numberOfPages = (
  state: 'closed' | 'open' | 'all' = 'open',
  perPage: number,
  { total_count, closed_issues_count, open_issues_count }: any
) => {
  switch (state) {
    case 'closed':
      return Math.ceil(closed_issues_count / perPage);
    case 'open':
      return Math.ceil(open_issues_count / perPage);
    case 'all':
      return Math.ceil(total_count / perPage)
    default:
      return 0
  }
}
