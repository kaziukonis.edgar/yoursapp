import { ActionTypes, IssuesAction, IssuesState } from "./types";

const initialState: IssuesState = { loading: false, issues: [], numberOfPages: 0 };

export const issuesReducer = (state = initialState, action: IssuesAction): IssuesState => {
  switch (action.type) {
    case ActionTypes.REQUEST_GET_ISSUES:
      return {
        ...state,
        numberOfPages: 0,
        loading: true
      }
    case ActionTypes.SUCCESS_GET_ISSUES:
      return {
        ...state,
        loading: false,
        issues: action.data,
        numberOfPages: action.number_of_pages,
        error: undefined
      }
    case ActionTypes.ERROR_GET_ISSUES:
      return {
        ...state,
        loading: false,
        numberOfPages: 0,
        error: action.error
      };
    default:
      return state;
  }
};

export default issuesReducer
