import { getIssues } from "../../services/github";

export enum ActionTypes {
  REQUEST_GET_ISSUES = "REQUEST_GET_ISSUES",
  SUCCESS_GET_ISSUES = "SUCCESS_GET_ISSUES",
  ERROR_GET_ISSUES = "ERROR_GET_ISSUES",
}

type IssuesRequestDataExtended = Awaited<ReturnType<typeof getIssues>>["data"]

export interface IssuesAction {
  type: ActionTypes,
  data?: IssuesRequestDataExtended,
  number_of_pages?: number,
  error?: unknown
}

export interface IssuesState {
  readonly loading: boolean;
  readonly issues?: IssuesRequestDataExtended;
  numberOfPages: number | undefined;
  error?: unknown;
}


