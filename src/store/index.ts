import issuesReducer from './issues/reducers';
import repoReducer from './repo/reducers';
import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";

const rootReducer = combineReducers({
	issuesReducer,
	repoReducer
});

export const store = configureStore({
	reducer: rootReducer,
	middleware(getDefaultMiddleware) {
		return getDefaultMiddleware({
			serializableCheck: false
		})
	},
});
