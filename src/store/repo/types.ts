import { getRepo } from "../../services/github";

export enum ActionTypes {
  REQUEST_GET_REPO = "REQUEST_GET_REPO",
  SUCCESS_GET_REPO = "SUCCESS_GET_REPO",
  ERROR_GET_REPO = "ERROR_GET_REPO",
}

interface DataExtension {
  total_count: number,
  closed_issues_count: number,
}

type ReposRequestDataExtended = Awaited<ReturnType<typeof getRepo>>["data"] & DataExtension

export interface RepoAction {
  type: ActionTypes,
  data?: ReposRequestDataExtended,
  error?: unknown
}

export interface RepoState {
  readonly loading: boolean;
  readonly repo?: ReposRequestDataExtended | null;
  error?: unknown
}


