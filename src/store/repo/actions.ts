import { Dispatch } from "react";
import { getIssues, getRepo } from "../../services/github";
import { ActionTypes, RepoAction } from "./types";

export const getRepoAction = (dispatch: Dispatch<RepoAction>) => async (owner: string, repository: string) => {
  dispatch({ type: ActionTypes.REQUEST_GET_REPO })

  try {
    const response = await getRepo(owner, repository);
    const { data } = await getIssues(owner, repository, 'all', 1, 1);
    const total_count = data.length > 0 ? data[0].number : 0
    return dispatch({
      type: ActionTypes.SUCCESS_GET_REPO,
      data: { 
        ...response.data,         
        total_count,
        closed_issues_count: total_count - response.data.open_issues_count }
    });
  } catch (err: any) {
    return dispatch({ type: ActionTypes.ERROR_GET_REPO, error: err });
  }
}
