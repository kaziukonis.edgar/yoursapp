import { ActionTypes, RepoAction, RepoState } from "./types";

const initialState: RepoState = { loading: false, repo: null, error: null };

export const repoReducer = (state = initialState, action: RepoAction): RepoState => {
  switch (action.type) {
    case ActionTypes.REQUEST_GET_REPO:
      return {
        ...state,
        loading: true
      }
    case ActionTypes.SUCCESS_GET_REPO:
      return {
        ...state,
        loading: false,
        repo: action.data,
        error: undefined
      }
    case ActionTypes.ERROR_GET_REPO:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
};

export default repoReducer
