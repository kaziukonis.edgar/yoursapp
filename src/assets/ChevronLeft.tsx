import { SVGProps } from 'react';

function ChevronLeft(props: SVGProps<SVGSVGElement>) {
	return (
		<svg width="5" height="8" viewBox="0 0 5 8" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
			<path d="M1.89189 4L5 7.06667L4.05405 8L0 4L4.05405 0L5 0.933333L1.89189 4Z" fill="#E6E7E9" />
		</svg>
	);
}

export default ChevronLeft;