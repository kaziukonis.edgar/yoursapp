import { SVGProps } from 'react';

function ChevronLeft(props: SVGProps<SVGSVGElement>) {
	return (
		<svg width="5" height="8" viewBox="0 0 5 8" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M3.10811 4L0 7.06667L0.945946 8L5 4L0.945946 0L0 0.933333L3.10811 4Z" fill="#E6E7E9" />
		</svg>
	);
}

export default ChevronLeft;
