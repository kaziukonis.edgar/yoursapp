import { SVGProps } from 'react';

function Check(props: SVGProps<SVGSVGElement>) {
    return (
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path opacity="0.5" d="M16.6665 1.66669H3.33317C2.4165 1.66669 1.6665 2.41669 1.6665 3.33335V18.3334L4.99984 15H16.6665C17.5832 15 18.3332 14.25 18.3332 13.3334V3.33335C18.3332 2.41669 17.5832 1.66669 16.6665 1.66669ZM16.6665 13.3334H4.99984L3.33317 15V3.33335H16.6665V13.3334Z" fill="#E6E7E9" />
        </svg>
    );
}

export default Check;
