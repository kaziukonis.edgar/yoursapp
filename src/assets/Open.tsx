import { SVGProps } from 'react';

function Check(props: SVGProps<SVGSVGElement>) {
	return (
		<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
			<circle cx="9" cy="9" r="8.5" stroke="#0DD9A8" />
			<circle cx="9" cy="9" r="2" fill="#0DD9A8" />
		</svg>
	);
}

export default Check;
