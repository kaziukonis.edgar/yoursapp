import styles from './App.module.scss';
import classNames from 'classnames/bind'
import Home from './pages/Home';
import Navbar from './components/Navbar';
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom";
import Issues from './pages/Issues';

const cx = classNames.bind(styles);

function App() {
  return (
    <div className={cx('App')}>
      <Navbar />
      <Router>
        <Routes>
          <Route element={<Home />} path="/" />
          <Route element={<Issues />} path="/issues" />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
