import styles from './Tooltip.module.scss';
import classNames from 'classnames/bind'

const cx = classNames.bind(styles);

interface Props {
  message: string,
  type: 'error',
  children?: React.ReactNode;
  visible?: boolean;
}

function Tooltip(props: Props) {
  const { message, type, children, visible } = props;

  return (
    <div className={cx('Tooltip')}>
      {children}
      {
        visible &&
        <div className={cx('Tooltip__tooltip')}>
          <div className={cx(['Tooltip__message', `Tooltip__message--${type}`])}>
            <div className={cx(['Tooltip__tail', 'Tooltip__tail--main'])} />
            <div className={cx(['Tooltip__tail', 'Tooltip__tail--cover'])} />
            {message}
          </div>
        </div>
      }
    </div>
  );
}

export default Tooltip;
