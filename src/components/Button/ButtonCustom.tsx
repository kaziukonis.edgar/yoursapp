import styles from './ButtonCustom.module.scss';
import classNames from 'classnames/bind'

const cx = classNames.bind(styles);

interface Props {
  label?: string,
  children?: React.ReactNode;
  className?: string;
  disabled?: boolean;
  onClick: () => void;
}

function ButtonCustom(props: Props) {
  const { className, children, disabled } = props;

  return (
    <button
      onClick={() => !disabled && props.onClick()}
      className={cx(['ButtonCustom', className, { 'ButtonCustom__disabled': disabled }])}
    >
      {children}
    </button>
  );
}

export default ButtonCustom;
