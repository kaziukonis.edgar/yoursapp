import styles from './Pagination.module.scss';
import classNames from 'classnames/bind'
import ChevronLeft from '../../assets/ChevronLeft';
import ChevronRight from '../../assets/ChevronRight';
import { useState } from 'react';
import { pagination } from './utils';

const cx = classNames.bind(styles);

interface Props {
	pagesCount: number,
	onChange: (page: number) => void,
	selectedPage?: number
}

// TODO: improve accessibility by replacing div's with buttons
function Pagination(props: Props) {
	const [pageSelected, setPageSelected] = useState<number>(props.selectedPage || 1)

	const changePageByStep = (add = true) => {
		setPageSelected((currentPage: number) => {
			if ((!add && currentPage > 1) || (add && currentPage < props.pagesCount)) {
				const selectedPage = add ? (currentPage + 1) : (currentPage - 1)
				props.onChange(selectedPage);

				return selectedPage
			}

			return currentPage
		})
	}
	const pages = pagination(pageSelected, props.pagesCount)
	return (
		<div className={cx('Pagination')}>
			<div
				className={cx(['Pagination__item', 'Pagination__item--previous'])}
				onClick={() => changePageByStep(false)}
			>
				<ChevronLeft />
				{"Previous"}
			</div>
			{
				pages.map(pageNumber => (
					pageNumber === '...' ?
						<span className={cx('Pagination__dots')}>
							...
						</span>
						:
						<div
							key={pageNumber}
							onClick={() => setPageSelected(Number(pageNumber))}
							className={cx([
								'Pagination__item',
								'Pagination__item--page-number',
								{ 'Pagination__item--page-number--active': pageSelected === pageNumber }
							])}>
							{pageNumber}
						</div>
				))
			}
			<div
				className={cx(['Pagination__item', 'Pagination__item--next'])}
				onClick={() => changePageByStep(true)}
			>
				{"Next"}
				<ChevronRight />
			</div>
		</div>
	);
}

export default Pagination;
