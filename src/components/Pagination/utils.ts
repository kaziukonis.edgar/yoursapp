export function pagination(selectedPage: number, numberOfPages: number) {
    const delta = 2,
        left = selectedPage - delta,
        right = selectedPage + delta + 1,
        range = [],
        rangeWithDots = [];
    let l;

    for (let i = 1; i <= numberOfPages; i++) {
        if (i == 1 || i == numberOfPages || i >= left && i < right) {
            range.push(i);
        }
    }

    for (let i of range) {
        if (l) {
            if (i - l === 2) {
                rangeWithDots.push(l + 1);
            } else if (i - l !== 1) {
                rangeWithDots.push('...');
            }
        }
        rangeWithDots.push(i);
        l = i;
    }

    return rangeWithDots;
}