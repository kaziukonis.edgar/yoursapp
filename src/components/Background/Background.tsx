import styles from './Background.module.scss';
import classNames from 'classnames/bind'

const cx = classNames.bind(styles);

function Background() {
	return (
		<div className={cx('Background')}>
			<div className={cx('Background__ellipse--1')} />
			<div className={cx('Background__ellipse--2')} />
			<div className={cx('Background__ellipse--3')} />
			<div className={cx('Background__ellipse--4')} />
			<div className={cx('Background__ellipse--5')} />
		</div>
	);
}

export default Background;
