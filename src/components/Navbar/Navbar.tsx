import styles from './Navbar.module.scss';
import classNames from 'classnames/bind'
import Logo from '../../assets/Logo';

const cx = classNames.bind(styles);

function Navbar() {
	return (
		<div className={cx('Navbar')}>
			<Logo className={cx('Navbar__logo')} />
		</div>
	);
}

export default Navbar;
