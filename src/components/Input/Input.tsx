import styles from './Input.module.scss';
import classNames from 'classnames/bind'
import Check from '../../assets/Check';
import { ChangeEvent } from 'react';

const cx = classNames.bind(styles);

interface Props {
  name: string,
  label?: string
  placeholder?: string;
  required?: boolean;
  isError?: boolean;
  isValid?: boolean;
  onChange: (value: string) => void;
}

function Input(props: Props) {
  const {
    label,
    name,
    placeholder,
    required,
    isError,
    isValid,
    onChange,
  } = props

  const onValueChange = (event: ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value)
  }

  return (
    <div className={cx('Input')}>
      {
        !!label &&
        <label
          className={cx('Input__label')}
          htmlFor={name}
        >
          {label}
          {
            !!required &&
            <span className={cx('Input__label--required')}>*</span>
          }
        </label>
      }
      {
        isValid &&
        <div className={cx('Input__field-valid-icon')}>
          <Check />
        </div>
      }
      <input
        placeholder={placeholder}
        className={cx(['Input__field', { 'Input__field--error': isError && !isValid }])}
        id={name}
        onChange={onValueChange}
      />
    </div>
  );
}

export default Input;
